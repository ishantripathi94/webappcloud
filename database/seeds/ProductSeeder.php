<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($count=0; $count<=5; $count++) {
            DB::table('products')->insert(
                [
                    'name' => Str::random(10),
                    'code' => Str::random(5)
                ]
            );
        }

    }
}
