@extends('master')
@section('css')
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/styles.css">
@endsection
@section('content')
    <div class="content">
        @if(session()->has('status'))
            <div class="bg-info">
                {{ session()->get('status') }}
            </div>
        @endif
        <div id="scanArea" style="width:300px; height:300px;"></div>
        <div class="button">
            <a class="btn btn-primary" id="scanButton">Scan QR code directly</a>
        </div>
        <div id="scanResult" class="d-none">
            <form method="POST" action="{{ url('/scanCode')  }}" class="form-group">
                @csrf
                <div>
                    <input type="text" name="code" id="scanResultCode">
                </div>
                <div>
                    <button type="submit" id="scanResultSubmit" class="btn btn-primary"></button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript" src="/js/webcam.min.js"></script>
    <script type="text/javascript" src="/js/qr/grid.js"></script>
    <script type="text/javascript" src="/js/qr/version.js"></script>
    <script type="text/javascript" src="/js/qr/detector.js"></script>
    <script type="text/javascript" src="/js/qr/formatinf.js"></script>
    <script type="text/javascript" src="/js/qr/errorlevel.js"></script>
    <script type="text/javascript" src="/js/qr/bitmat.js"></script>
    <script type="text/javascript" src="/js/qr/datablock.js"></script>
    <script type="text/javascript" src="/js/qr/bmparser.js"></script>
    <script type="text/javascript" src="/js/qr/datamask.js"></script>
    <script type="text/javascript" src="/js/qr/rsdecoder.js"></script>
    <script type="text/javascript" src="/js/qr/gf256poly.js"></script>
    <script type="text/javascript" src="/js/qr/gf256.js"></script>
    <script type="text/javascript" src="/js/qr/decoder.js"></script>
    <script type="text/javascript" src="/js/qr/qrcode.js"></script>
    <script type="text/javascript" src="/js/qr/findpat.js"></script>
    <script type="text/javascript" src="/js/qr/alignpat.js"></script>
    <script type="text/javascript" src="/js/qr/databr.js"></script>
    <script type="text/javascript" src="/js/effects.js"></script>
@endsection