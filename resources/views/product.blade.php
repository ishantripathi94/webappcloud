@extends('master')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
@endsection
@section('content')
    <div class="row">
        <div class="col-md">
            <div>
                <form method="POST" action="{{ url('productAdd')  }}" class="form-inline">
                    @csrf
                    <div class="form-group" style="margin: 4px">
                        <label for="name">Product Name:</label>
                        <input type="text" name="name" class="form-control" id="name">
                    </div>
                    <div class="form-group" style="margin: 4px">
                        <label for="code">Product Code:</label>
                        <input type="text" name="code" class="form-control" id="code">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <div>
                        <a class="btn btn-warning pull-right" href="{{ url('/logout') }}" style="margin: 30px ">Logout</a>
                    </div>
                </form>
            </div>
            <div style="margin: 50px">
                <table id="productTable" class="table table-bordered">
                    <thead>
                        <th class="text-center">Name</th>
                        <th class="text-center">Code</th>
                        <th class="text-center">Count</th>
                    </thead>
                    <tbody>
                    @foreach($productLists as $product)
                        <tr>
                            <td>{{ $product['name'] }}</td>
                            <td class="text-center">{{ $product['code'] }}</td>
                            <td class="text-center">{{ $product['count'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#productTable').DataTable();
        })
    </script>
@endsection