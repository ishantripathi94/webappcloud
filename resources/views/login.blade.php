@extends('master')
@section('css')
    <style>
        form > div {
            margin: 20px;
        }
    </style>
@endsection
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md" style="margin: 10%">
                <div class="text-center h4">Sign In</div>
                <form method="POST" action="{{ url('logincheck')  }}" class="form-group">
                    @csrf
                    <div>
                        <label for="email">Email:</label>
                        <input type="email" name="email" class="form-control" id="email">
                    </div>
                    <div>
                        <label for="password">Password:</label>
                        <input type="password" name="password" class="form-control" id="password">
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection