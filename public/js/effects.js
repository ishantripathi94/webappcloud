$(document).ready(function () {

	Webcam.attach('#scanArea');

	$('#scanButton').click(function () {
		take_snapshot();
	});

	qrcode.callback = showInfo;

});

function take_snapshot() {
	Webcam.snap(function (dataUrl) {
		qrCodeDecoder(dataUrl);
	});
}

// decode the img
function qrCodeDecoder(dataUrl) {
	qrcode.decode(dataUrl);
}

// show info from qr code
function showInfo(data) {
	$('#scanResultCode').val(data);
	$('#scanResultSubmit').click();
}
