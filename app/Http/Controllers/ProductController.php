<?php

namespace App\Http\Controllers;


use App\Model\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Jenssegers\Agent\Agent;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('productView', 'productAdd');
    }

    /**
     * @return Application|Factory|View|\Illuminate\View\View
     */
    public function productView()
    {
        $productList = Product::all();

        return view('product')->with(['productLists' => $productList]);
    }

    /**
     * @return RedirectResponse
     */
    public function productAdd()
    {
        $rules = [
            'name' => 'required|max:200',
            'code' => 'required|max:50',
        ];
        $v = Validator::make(Request::all(), $rules);

        if ($v->fails()) {
            Request::flash();
            return Redirect::to('product')
                ->withInput()
                ->withErrors($v->messages());
        }

        $product = new Product();
        $product->name = trim(Request::get('name'));
        $product->code = trim(Request::get('code'));
        $product->save();

        return Redirect::to('product');
    }

    /**
     * @param string $code
     *
     * @return JsonResponse
     */
    public function incrementProductCount(string $code)
    {
        $code = filter_var(trim($code), FILTER_SANITIZE_STRING);
        if ($code === '') {
            return Response::json('Code can not be empty',400);
        }

        $product = Product::firstWhere('code', $code);
        if(is_null($product)) {
            return Response::json('Product with code not found');
        }

        ++$product->count;
        $product->save();

        return Response::json('Product incremented by 1');
    }

    /**
     * @return \Illuminate\Contracts\View\View|JsonResponse
     */
    public function scanCode()
    {
        $agent = new Agent();
        if(!$agent->isPhone()) {
            return Response::json('You are not on Mobile device. Please open the URL on mobile to scan code');
        }

        return View::make('scan');
    }

    /**
     * @return RedirectResponse
     */
    public function scanValueUpdate()
    {
        $code = filter_var(trim(Request::get('code')), FILTER_SANITIZE_STRING);
        if ($code === '') {
            session()->flash('status', 'Code can not be empty. Please rescan properly');
        } elseif ($code === 'error decoding QR Code') {
            session()->flash('status', 'QR code scan is not clear to decode. Please scan QR properly again');
        } else {
            $product = Product::firstWhere('code', $code);
            if (is_null($product)) {
                session()->flash('status', 'Invalid Code please scan QR again.');
            } else {
                ++$product->count;
                $product->save();
                session()->flash('status', 'Scan Success. Product count increased by 1');
            }
        }

        return redirect()->route('scanUI');
    }
}
