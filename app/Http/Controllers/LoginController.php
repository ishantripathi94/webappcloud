<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class LoginController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\View|RedirectResponse
     */
    public function login()
    {
        if (Auth::check()) {
            return Redirect::to('product');
        }
        return View::make('login');
    }

    /**
     * @return RedirectResponse
     */
    public function loginCheck()
    {
        $rules = [
            'email' => 'required|max:25',
            'password' => 'required|max:25',
        ];
        $v = Validator::make(Request::all(), $rules);

        if ($v->fails()) {
            Request::flash();
            return Redirect::to('login')
                ->withInput()
                ->withErrors($v->messages());
        }

        $userdata = [
            'email' => Request::get('email'),
            'password' => Request::get('password'),
        ];

        if (Auth::attempt($userdata)) {
            return Redirect::to('product');
        }

        return Redirect::to('login')->withErrors('Incorrect login details');
    }

    /**
     * @return RedirectResponse
     */
    public function logout()
    {
        Auth::logout ();
        return Redirect::to('login');
    }
}
