<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public const CREATED_AT = 'addedOn';
    public const UPDATED_AT = 'updatedOn';
}
